package pl.codementors;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application{

    public static void main(String[] args) { launch(args);}

    @Override
    public void start(Stage stage) throws Exception {

        URL fxml = this.getClass().getResource("/pl/codementors/view/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.view.messages.ents_list_msg");

        Parent root = FXMLLoader.load(fxml, rb);

        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("/pl/codementors/view/css/ents_list.css");
        stage.setTitle(rb.getString("applicationTitle"));
        stage.setScene(scene);
        stage.show();

    }
}
