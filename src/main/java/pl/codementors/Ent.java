package pl.codementors;

import java.io.Serializable;

public class Ent implements Serializable {

    private int dateOfPlanting;
    private int heightOfEnt;
    private String speciesOfEnt;
    public enum type {
        Iglasty,
        Lisciasty
    }
    private type type;

    public Ent(){

    }

    public Ent(int dateOfPlanting, int heightOfEnt, String speciesOfEnt, type type) {
        this.dateOfPlanting = dateOfPlanting;
        this.heightOfEnt = heightOfEnt;
        this.speciesOfEnt = speciesOfEnt;
        this.type = type;

    }

    public int getDateOfPlanting() {
        return dateOfPlanting;
    }

    public void setDateOfPlanting(int dateOfPlanting) {
        this.dateOfPlanting = dateOfPlanting;
    }

    public int getHeightOfEnt() {
        return heightOfEnt;
    }

    public void setHeightOfEnt(int heightOfEnt) {
        this.heightOfEnt = heightOfEnt;
    }

    public String getSpeciesOfEnt() {
        return speciesOfEnt;
    }

    public void setSpeciesOfEnt(String speciesOfEnt) {
        this.speciesOfEnt = speciesOfEnt;
    }

    public Ent.type getType() {
        return type;
    }

    public void setType(Ent.type type) {
        this.type = type;
    }
}
