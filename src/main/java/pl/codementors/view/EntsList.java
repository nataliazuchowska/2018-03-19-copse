package pl.codementors.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.Ent;

import java.io.*;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EntsList implements Initializable{

    private static final Logger log = Logger.getLogger(EntsList.class.getCanonicalName());

    @FXML
    private TableView entsTable;

    @FXML
    private TableColumn<Ent, Integer> dateOfPlantingColumn;

    @FXML
    private TableColumn<Ent, Integer> heightOfEntColumn;

    @FXML
    private TableColumn<Ent, String> speciesOfEntColumn;

    @FXML
    private TableColumn<Ent, Ent.type> typeColumn;

    @FXML
    private ObservableList<Ent> ents = FXCollections.observableArrayList();

    private ResourceBundle rb;
    @FXML
    private ProgressBar progress;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    entsTable.setItems(ents);
    rb = resourceBundle;
    progress.setProgress(0);

        dateOfPlantingColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        dateOfPlantingColumn.setOnEditCommit(event -> event.getRowValue().setDateOfPlanting(event.getNewValue()));

        heightOfEntColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        heightOfEntColumn.setOnEditCommit(event -> event.getRowValue().setHeightOfEnt(event.getNewValue()));

        speciesOfEntColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        speciesOfEntColumn.setOnEditCommit(event -> event.getRowValue().setSpeciesOfEnt(event.getNewValue()));

        typeColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.type.values()));
        typeColumn.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));


    }

    @FXML
    private void open(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }

    private void open(File file) {
        OpenWorker openWorker = new OpenWorker(ents, file);
        progress.progressProperty().bind(openWorker.progressProperty());
        new Thread(openWorker).start();
    }

    @FXML
    private void save(ActionEvent event) {  FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            save(file);
        }
    }

    private void save (File file){
     SaveWorker saveWorker = new SaveWorker(ents, file);
     progress.progressProperty().bind(saveWorker.progressProperty());
     Thread thread = new Thread(saveWorker);
     thread.start();
    }

    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void plant (ActionEvent event){
        ents.add(new Ent());
    }

    @FXML
    private void cut(ActionEvent event){
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }
    }

    @FXML
    public void root(ActionEvent actionEvent) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        final int height = 10;
        Ent selectedEnt = (Ent) entsTable.getSelectionModel().getSelectedItem();
        String species = selectedEnt.getSpeciesOfEnt();
        Ent.type type = selectedEnt.getType();
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.add(new Ent(year, height, species, type));
        }
    }
}