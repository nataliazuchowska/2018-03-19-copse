package pl.codementors.view;

import javafx.concurrent.Task;
import pl.codementors.Ent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveWorker extends Task{

    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());

    private Collection<Ent> ents;

    private File file;

    public SaveWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        int i=0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(ents.size());
            for (Ent ent : ents) {
                oos.writeObject(ent);
                Thread.sleep(700);
                ++i;
                updateProgress(i, ents.size());
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}
